var gulp = require('gulp');
var data = require('gulp-data');
var stylus = require('gulp-stylus');
var pug = require('gulp-pug');
const imagemin = require('gulp-imagemin');
const htmlmin = require('gulp-htmlmin');
let cleanCSS = require('gulp-clean-css');




// Get one .styl file and render
gulp.task('one', function () {
        return gulp.src('src/css/*.styl')
            .pipe(stylus())
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(gulp.dest('build/'));
    //Hace el HTML con STYL
    }
);
gulp.task('two', function () {
        return gulp.src('src/pages/*.pug')
            .pipe(pug())
            .pipe(htmlmin({ collapseWhitespace: true }))
            .pipe(gulp.dest('build/'));
    }
);

gulp.task('three', function () {
        return gulp.src('src/images/*')
            .pipe(imagemin())
            .pipe(gulp.dest('build/'))
    }
);